
import ContactUs from "./components/ContactUs";
import AboutUs from "./components/AboutUs";

export default [

    { path: '/aboutUs', component: AboutUs },
    { path: '/contactUs', component: ContactUs }
]

